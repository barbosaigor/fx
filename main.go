package main

import (
	"sandbox/fx/fx/module"

	"go.uber.org/fx"
)

func main() {
	_ = fx.New(module.NewRabbit())
}
