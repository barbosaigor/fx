package module

import (
	"sandbox/fx/database"
	"sandbox/fx/provider/mongo"

	"go.uber.org/fx"
)

func NewMongo() fx.Option {
	return fx.Options(
		fx.Provide(mongo.New),
		fx.Supply("mongo:localhost:27017"),
		fx.Provide(func(m *mongo.Mongo) database.DB { return m }),
	)
}
