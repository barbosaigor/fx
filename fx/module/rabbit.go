package module

import (
	"sandbox/fx/provider/rabbitmq"

	"go.uber.org/fx"
)

func NewRabbit() fx.Option {
	return fx.Options(
		NewMongo(),
		fx.Provide(rabbitmq.New),
		fx.Invoke(func(mq *rabbitmq.RabbitMQ) {
			mq.Publish("event-categoried", `{"name": "test"}`)
			mq.Receive("event-persisted", `{"path": "/bla/123"}`)
		}),
	)
}
