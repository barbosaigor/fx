package mongo

import "fmt"

type Mongo struct {
	URL string
}

func New(url string) *Mongo {
	return &Mongo{URL: url}
}

func (m *Mongo) Query(query interface{}) error {
	fmt.Printf("Querying [%v]: %v\n", m.URL, query)
	return nil
}
