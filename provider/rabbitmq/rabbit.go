package rabbitmq

import (
	"fmt"
	"sandbox/fx/database"
)

type RabbitMQ struct {
	Count int
	DB    database.DB
}

func New(db database.DB) *RabbitMQ {
	return &RabbitMQ{DB: db}
}

func (mq *RabbitMQ) Publish(key, value string) error {
	fmt.Printf("Publish %s: %s\n", key, value)
	return mq.DB.Query(fmt.Sprintf("removing from storage event %s: %s", key, value))
}

func (mq *RabbitMQ) Receive(key, value string) error {
	fmt.Printf("Receive %s: %s\n", key, value)
	return mq.DB.Query(fmt.Sprintf("storing event %s: %s", key, value))
}
