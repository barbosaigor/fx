package queue

type Publisher interface {
	Publish(key, value string) error
}

type Receiver interface {
	Receive(key, value string) error
}

type Queue interface {
	Publisher
	Receiver
}
