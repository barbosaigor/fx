package database

type DB interface {
	Query(query interface{}) error
}
